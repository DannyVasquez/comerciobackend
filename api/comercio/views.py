# GET (Obtener 1 o muchos objetos)
# --> list (lista los objetosobjetos)
# --> retrieve (lista un objetos)
# POST (Crear 1 o muchos objetos)
# --> create (crea un objeto)
# PUT, PATCH (Modificar Objetos
# --> update (put: actualiza un objeto)
# --> partial_update (path: actualiza un objeto para solo los campos que enviamos)
# DELETE (Eliminar 1 o mas objetos)
# --> destroy (borra un objeto)
from rest_framework import viewsets, status

from api.comercio.serializers import LocalSerializer, SeccionSerializer, ProductoSerializer, OfertaProductoSerializer, \
    ValoracionSerializer
from comercio.models import Local, Valoracion, OfertaProducto, Producto, Seccion
from rest_framework.decorators import action # isntalar
from rest_framework.response import Response
from django.conf.urls import url
from rest_framework_swagger.views import get_swagger_view


class LocalViewSet(viewsets.ModelViewSet):
    queryset = Local.objects.all()
    serializer_class = LocalSerializer


class SeccionViewSet(viewsets.ModelViewSet):
    queryset = Seccion.objects.all()
    serializer_class = SeccionSerializer

    #EXTRAE TODOS LOS SECCIONES QUE TIENE UN DETERMINADO LOCAL(ID)
    @action(methods=['get'], url_path='--search-Seccion_by_LocalID/(?P<Local_ID>[^/]+)', detail=False)
    def search_Seccion_by_LocalID(self, request, *arg, **kwargs):
        try:
            localid = kwargs.get('Local_ID', None)
            lista = Seccion.objects.filter(local__id=localid)
            objetoSerializable = SeccionSerializer(lista, many=True)
            return Response(objetoSerializable.data)
        except Exception as e:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ProductoViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer


class OfertaProductoViewSet(viewsets.ModelViewSet):
    queryset = OfertaProducto.objects.all()
    serializer_class = OfertaProductoSerializer


class ValoracionViewSet(viewsets.ModelViewSet):
    queryset = Valoracion.objects.all()
    serializer_class = ValoracionSerializer
